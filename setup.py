import setuptools

setuptools.setup (
    name="mypypipackage",
    version="0.0.3",
    author="brian.lim",
    author_email="kokhoui83@gmail.com",
    description="A small example package",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8'
)