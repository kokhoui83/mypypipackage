# My PyPI Package
- Example pypi package creation and publishing to Gitlab package registry using Gitlab CI/CD

## Versioning
- Manual versioning via `setup.py`

## Installing package
- using pip install
```
pip install mypypipackage --extra-index-url https://__token__:<your_personal_token>@gitlab.com/api/v4/projects/31126792/packages/pypi/simple
```
- using `.pypirc`
```
[gitlab]
repository = https://gitlab.com/api/v4/projects/31126792/packages/pypi
username = __token__
password = <your personal access token>
```